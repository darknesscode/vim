#!/bin/bash
#  ____             _                         ____          _
# |  _ \  __ _ _ __| | ___ __   ___  ___ ___ / ___|___   __| | ___
# | | | |/ _' | '__| |/ / '_ \ / _ \/ __/ __| |   / _ \ / _' |/ _ \
# | |_| | (_| | |  |   <| | | |  __/\__ \__ \ |__| (_) | (_| |  __/
# |____/ \__,_|_|  |_|\_\_| |_|\___||___/___/\____\___/ \__,_|\___|
# -----------------------------------------------------------------
# https://darkncesscode.xyz
# https://github.com/codedarkness
# -----------------------------------------------------------------
#
#        FILE: vim-install.sh
#       USAGE: ./vim-install.sh
#
# DESCRIPTION: install or update vim, copy custome files and
#	       plugins
#
#      AUTHOR: DarknessCode
#       EMAIL: achim@darknesscode.xyz
#
#     CREATED: 05-15-2020
#
# -----------------------------------------------------------------

install-neovim() {
	echo ""
	echo " Installing NeoVim"
	echo " Arch Liux | Debian | Void Linux"
	echo ""
	sleep 2

	while true; do
		read -p " Install NeoVim [y - n] : " yn
		case $yn in
			[Yy]* )
				if ! location="$(type -p "vim")" || [ -z "vim" ]; then

					# check if pacman is installed
					if which pacman > /dev/null 2>&1; then

						sudo pacman -S --noconfirm neovim
						python3 -m pip install --user --upgrade pynvim

					# check if apt is installed
					elif which apt > /dev/null 2>&1; then

						sudo apt install -y neovim
						python3 -m pip install --user --upgrade pynvim

					# check if xbps is installed
					elif which xbps-install > /dev/null 2>&1; then

						sudo xbps-install -Sy neovim
						python3 -m pip install --user --upgrade pynvim

					else

						echo " Your system is not supported"
					fi


					else
						echo " Nothing to do! NeoVim is installed in your System"
				fi ; break ;;
			[Nn]* )
				break ;;
			* ) echo "Please answer yes or no." ;;
		esac
	done

	echo ""

	while true; do
		read -p " Copy NeoVim Config Files [y - n] : " yn
		case $yn in
			[Yy]* )
				cp -af config-files/configs/nvim $HOME/.config &&
				echo " neovim config files were copied" || echo " Sssshhhhh!!"
				echo ""; break ;;
			[Nn]* )
				break ;;
			* ) echo "Please answer yes or no." ;;
		esac
	done
	
	echo ""
	
}

install-vim() {
	echo ""
	echo " Installing Vim"
	echo " Arch Liux | Debian | Void Linux"
	echo ""
	sleep 2

	while true; do
		read -p " Install Vim [y - n] : " yn
		case $yn in
			[Yy]* )
				if ! location="$(type -p "vim")" || [ -z "vim" ]; then

					# check if pacman is installed
					if which pacman > /dev/null 2>&1; then

						sudo pacman -S --noconfirm vim

					# check if apt is installed
					elif which apt > /dev/null 2>&1; then

						sudo apt install -y vim-nox

					# check if xbps is installed
					elif which xbps-install > /dev/null 2>&1; then

						sudo xbps-install -Sy vim-huge

					else

						echo " Your system is not supported"
					fi

					else
						echo " Nothing to do! Vim is installed in your System"
				fi ; break ;;
			[Nn]* )
				break ;;
			* ) echo "Please answer yes or no." ;;
		esac
	done

	echo ""

	while true; do
		read -p " Copy Vim config files [y - n] : " yn
		case $yn in
			[Yy]* )
				## autoload directory
				### Check for dir, if not found create it using the mkdir ###
				dldir="$HOME/.vim/autoload"
				[ ! -d "$dldir" ] && mkdir -p "$dldir" &&
				echo " autoload directory was created" || echo " $dldir already exist!"
				echo ""

				## UltiSnips directory
				### Check for dir, if not found create it using the mkdir ###
				dldir2="$HOME/.vim/UltiSnips"
				[ ! -d "$dldir2" ] && mkdir -p "$dldir2" &&
				echo " UltiSnips directory was created" || echo " $dldir2 already exist!"
				echo ""

				## bundle directory
				### Check for dir, if not found create it using the mkdir ###
				dldir3="$HOME/.vim/bundle"
				[ ! -d "$dldir3" ] && mkdir -p "$dldir3" &&
				echo " bundle directory was created" || echo " $dldir3 already exist!"
				echo ""

				cp -af config-files/configs/vim/plug.vim $HOME/.vim/autoload/ &&
				echo " plug.vim was copied" || echo " Sssshhhhh!!"
				echo ""

				cp -af config-files/configs/vim/all.snippets $HOME/.vim/UltiSnips &&
				echo " general snippets file was copied" || echo " Not again!!!"
				echo ""

				cp -ar config-files/configs/vim/vimrc $HOME/.vimrc &&
				echo " vimrc was copied" || echo " Don't worry is your system, not you!"
				echo "";
				break ;;
			[Nn]* )
				break 0 ;;
			* ) echo "Please answer yes or no." ;;
		esac
	done

	echo ""
}

press_enter() {
	echo ""
	echo -n " Press Enter To Continue"
	read
	clear
}

incorrect_selection() {
	echo " Incorrect selection! try again"
}

until [ "$selection" = "0" ]; do
	clear
	echo ""
	echo " DarknessCode"
	echo "        _            "
	echo "       (_)           "
	echo " __   ___ _ __ ___   "
	echo " \ \ / / | '_ ' _ \  "
	echo "  \ V /| | | | | | | "
	echo "   \_/ |_|_| |_| |_| "
	echo ""
	echo " The ubiquitous text editor"
	echo ""
	echo " 1 - Install NeoVim"
	echo " 2 - Install Vim"
	echo ""
	echo " 0 - Exit"
	echo ""
	echo -n " Enter selection [1 - 0] : "
	read selection
	echo ""

	case $selection in
		1) clear; install-neovim	; press_enter ;;
		2) clear; install-vim  		; press_enter ;;
		0) clear; exit ;;
		*) clear; incorrect_selection ; press_enter ;;
	esac
done
